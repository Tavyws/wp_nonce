# Wordpress Nonce #

This is a test project that demonstrates WP nonces. It runs on PHP7.x and it was tests on PHP7.2.

This paragraph would define what WP Nonces exactly do.

### Structure of the project ###

vendor - composer packages
.gitignore - ignored files - PHPStorm folder, composer packages;
tests - tests folder
src - project files


### How tests are run ###

composer.phar package included for convenience

Change directory to the project directory and run:
php composer.phar install
vendor/bin/phpunit tests

Or on windows:
php composer.phar install
vendor/bin/phpunit.bat tests

### Project plan ###

* ~~TASK-1 Create the .gitignore, composer.json and the README.md file~~
* ~~TASK-2 Write the *integration* tests that the project should suffice~~
* ~~TASK-3 Write the project files and make sure the tests are passing~~

### Benevolent dictator ###

The benevolent dictator of the project would be me, Octavian Paralescu, a full-stack web developer based in Romania, email address: me@octav.info and Linkedin profile: https://www.linkedin.com/in/paralescuoctavian.
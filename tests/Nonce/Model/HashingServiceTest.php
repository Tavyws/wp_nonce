<?php

namespace WpNonce\Tests\Nonce\Model;

use PHPUnit\Framework\TestCase;
use WpNonce\Nonce\Model\HashingService;
use WpNonce\Nonce\Model\HashingServiceInterface;

/**
 * Class HashingServiceTest
 *
 * @package WpNonce\Tests\Nonce\Model
 */
class HashingServiceTest extends TestCase
{
    const CONTROL_STRING = 'a string';
    const CONTROL_HASH = 'f111255dcf7ca7db192d73090a8f5129';
    /**
     * The hashing service used with the tests
     *
     * @var HashingServiceInterface
     */
    private $_hashingService;

    /**
     * Sets up the HashingService
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->_hashingService = new HashingService('a-salt', 'md5');
    }

    /**
     * Tests hash generation
     *
     * @return string
     */
    public function testHash()
    {
        $hash = $this->_hashingService->hash(self::CONTROL_STRING);

        $this->assertEquals(self::CONTROL_HASH, $hash);

        return $hash;
    }

    /**
     * Tests hashEquals method
     *
     * @param string $hash Input hash
     *
     * @depends testHash
     *
     * @return void
     */
    public function testHashEquals(string $hash)
    {
        $this->assertTrue(
            $this->_hashingService->hashEquals($hash, self::CONTROL_HASH)
        );
    }
}

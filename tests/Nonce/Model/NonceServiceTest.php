<?php

namespace WpNonce\Tests\Nonce\Model;

use PHPUnit\Framework\TestCase;
use WpNonce\Auth\Entity\User;
use WpNonce\Nonce\Model\HashingService;
use WpNonce\Nonce\Model\NonceService;
use WpNonce\Nonce\Model\NonceServiceInterface;
use WpNonce\Session\Entity\Session;
use WpNonce\Session\Model\SessionContainer;

/**
 * NonnceService test
 *
 * @package WpNonce\Nonce\Model
 * @author  Octavian Paralescu <me@octav.info>
 * @license GPLv2 https://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */
class NonceServiceTest extends TestCase
{
    /**
     * The nonce service used with the tests
     *
     * @var NonceServiceInterface
     */
    private $_nonceService;

    /**
     * Sets up the HashingService
     *
     * @return void
     * @throws \Exception
     */
    protected function setUp(): void
    {
        parent::setUp();
        $userMock = $this->createConfiguredMock(
            User::class,
            ['getId' => 200]
        );
        $sessionMock = $this->createConfiguredMock(
            Session::class,
            ['getUser' => $userMock, 'getHash' => 'a-hash']
        );
        $sessionContainer = $this->createConfiguredMock(
            SessionContainer::class,
            ['getSession' => $sessionMock]
        );
        $this->_nonceService = new NonceService(
            60 * 60 * 24,
            $sessionContainer,
            $this->createConfiguredMock(
                HashingService::class,
                ['hash' => 'a hash with more than 20 chars', 'hashEquals' => true]
            ),
            new \DateTime('01/01/2019')
        );
    }

    /**
     * Tests nonce generation
     *
     * @return void
     */
    public function testGenerateNonce()
    {
        $nonce = $this->_nonceService->generateNonce('an action');

        $this->assertEquals('han 20 cha', $nonce);
    }

    /**
     * Tests nonce equals function
     *
     * @return void
     */
    public function testCheckNonce()
    {
        $nonceEquals = $this->_nonceService->checkNonce('han 20 cha', 'an action');

        $this->assertEquals(
            NonceService::NONCE_GENERATED_0_12_HOURS_AGO,
            $nonceEquals
        );
    }
}

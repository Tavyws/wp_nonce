<?php

namespace Tests\Integration;

require_once __DIR__ . "/../bootstrap.php";

use PHPUnit\Framework\TestCase;
use Symfony\Component\Yaml\Yaml;
use WpNonce\Application\Controller\ApplicationController;
use WpNonce\Application\Factory\ApplicationControllerFactory;
use WpNonce\Nonce\Model\NonceService;

/**
 * Class IntegrationTest
 *
 * @package Tests\Integration
 * @author  Octavian Paralescu <me@octav.info>
 * @license GPLv2 https://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */
class IntegrationTest extends TestCase
{
    /**
     * The YAML configuration file
     */
    const CONFIG_FILE = __DIR__ . DIRECTORY_SEPARATOR . 'config.yml';
    const USERNAME = 'a-user-name';
    const USERID = 200;
    const SESSION_KEY = 'a-session-key';
    const ACTION = 'an-action';
    /**
     * The parsed config
     *
     * @var array
     */
    private $_config;

    /**
     * An initial DateTime
     *
     * @var \DateTime
     */
    private $_initialDateTime;

    /**
     *  Sets up the application with the config
     *
     * @return void
     * @throws \Exception
     */
    protected function setUp(): void
    {
        parent::setUp();
        // Should load the config from the yml
        $this->_config = Yaml::parseFile(self::CONFIG_FILE);
        $this->_initialDateTime = new \DateTime('01/01/2019 04:00:00');
    }

    /**
     * Tests the logIn process
     *
     * @return ApplicationController
     *
     * @throws \Exception
     */
    public function testLogIn(): ApplicationController
    {
        $applicationController = ApplicationControllerFactory::create(
            $this->_config['session'],
            $this->_config['hasher'],
            $this->_config['nonce'],
            self::SESSION_KEY,
            $this->_initialDateTime
        );

        $loginResult = $applicationController->logIn(
            self::USERID,
            self::USERNAME
        );

        $this->assertTrue($loginResult, 'Could not login');

        return $applicationController;
    }

    /**
     * Verifies that the creation of a nonce works
     *
     * @param ApplicationController $applicationController The app controller
     *
     * @return  array
     * @depends testLogIn
     */
    public function testCreateNonce(ApplicationController $applicationController)
    : array
    {
        $nonce = $applicationController->createNonce(self::ACTION);

        $this->assertEquals('97576ce58c', $nonce);

        return ['app' => $applicationController, 'nonce' => $nonce];
    }

    /**
     * Verifies that the nonce after 14H
     *
     * @param array $dependencies The app and Nonce created in the previous tests
     *
     * @depends testCreateNonce
     * @return  void
     * @throws  \Exception
     */
    public function testVerifyNonce1224Valid(array $dependencies)
    : void
    {
        $newDateTime = $this->_initialDateTime->add(new \DateInterval("PT14H"));
        $newApp = $applicationController = ApplicationControllerFactory::create(
            $this->_config['session'],
            $this->_config['hasher'],
            $this->_config['nonce'],
            self::SESSION_KEY,
            $newDateTime
        );
        $newApp->logIn(
            self::USERID,
            self::USERNAME
        );
        $previousNonce = $dependencies['nonce'];
        $validationResult = $newApp->verifyNonce(
            $previousNonce,
            self::ACTION
        );

        $this->assertEquals(
            NonceService::NONCE_GENERATED_12_24_HOURS_AGO,
            $validationResult
        );
    }

    /**
     * Verifies that the nonce after 8H
     *
     * @param array $dependencies The app and Nonce created in the previous tests
     *
     * @depends testCreateNonce
     * @return  void
     * @throws  \Exception
     */
    public function testVerifyNonce012Valid(array $dependencies)
    : void
    {
        $newDateTime = $this->_initialDateTime->add(new \DateInterval("PT8H"));
        $newApp = $applicationController = ApplicationControllerFactory::create(
            $this->_config['session'],
            $this->_config['hasher'],
            $this->_config['nonce'],
            self::SESSION_KEY,
            $newDateTime
        );
        $newApp->logIn(
            self::USERID,
            self::USERNAME
        );
        $previousNonce = $dependencies['nonce'];
        $validationResult = $newApp->verifyNonce(
            $previousNonce,
            self::ACTION
        );

        $this->assertEquals(
            NonceService::NONCE_GENERATED_0_12_HOURS_AGO,
            $validationResult
        );
    }

    /**
     * Verifies that the nonce is valid in the same second
     *
     * @param array $dependencies The app and Nonce created in the previous tests
     *
     * @depends testCreateNonce

     * @return void
     */
    public function testVerifyNonceSameSecondValid(array $dependencies)
    : void
    {
        /**
         * App
         *
         * @var ApplicationController $applicationController
         */
        $applicationController = $dependencies['app'];

        $previousNonce = $dependencies['nonce'];
        $validationResult = $applicationController->verifyNonce(
            $previousNonce,
            self::ACTION
        );

        $this->assertEquals(
            NonceService::NONCE_GENERATED_0_12_HOURS_AGO,
            $validationResult
        );
    }

    /**
     * Verifies that the nonce after 48H
     *
     * @param array $dependencies The app and Nonce created in the previous tests
     *
     * @depends testCreateNonce
     * @return  void
     * @throws  \Exception
     */
    public function testVerifyNonce48HInvalid(array $dependencies)
    : void
    {
        $newDateTime = $this->_initialDateTime->add(new \DateInterval("PT48H"));
        $newApp = $applicationController = ApplicationControllerFactory::create(
            $this->_config['session'],
            $this->_config['hasher'],
            $this->_config['nonce'],
            self::SESSION_KEY,
            $newDateTime
        );
        $newApp->logIn(
            self::USERID,
            self::USERNAME
        );
        $previousNonce = $dependencies['nonce'];
        $validationResult = $newApp->verifyNonce(
            $previousNonce,
            self::ACTION
        );

        $this->assertEquals(
            NonceService::NONCE_INVALID,
            $validationResult
        );
    }

    /**
     * Verifies that the nonce after 48H
     *
     * @param array $dependencies The app and Nonce created in the previous tests
     *
     * @depends testCreateNonce
     * @return  void
     * @throws  \Exception
     */
    public function testVerifyNonceInvalid(array $dependencies)
    : void
    {
        /**
         * App
         *
         * @var ApplicationController $applicationController
         */
        $applicationController = $dependencies['app'];

        $erroneousNonce = 'other';
        $validationResult = $applicationController->verifyNonce(
            $erroneousNonce,
            self::ACTION
        );

        $this->assertEquals(
            NonceService::NONCE_INVALID,
            $validationResult
        );
    }
}
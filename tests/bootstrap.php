<?php

defined('DATA_FOLDER') ||
    define('DATA_FOLDER', __DIR__ . DIRECTORY_SEPARATOR . 'data');

require_once __DIR__ . '/../vendor/autoload.php'; // Composer packages
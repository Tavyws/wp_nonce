<?php

namespace WpNonce\Tests\Auth\Entity;


use PHPUnit\Framework\TestCase;
use WpNonce\Auth\Entity\User;

class UserTest extends TestCase
{
    /**
     * Disaster detection for getters
     *
     * @return void
     */
    public function testGetters(): void
    {
        $userId = 1;
        $username = 'username';
        $user = new User($userId, $username);

        $this->assertEquals($userId, $user->getId());
        $this->assertEquals($username, $user->getUsername());
    }
}

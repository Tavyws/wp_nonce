<?php

namespace WpNonce\Tests\Session\Model;

use PHPUnit\Framework\TestCase;
use WpNonce\Session\Entity\Session;
use WpNonce\Session\Model\SessionAdapterInterface;
use WpNonce\Session\Model\SessionContainer;

class SessionContainerTest extends TestCase
{
    /**
     * Session container being tested
     *
     * @var SessionContainer
     */
    private $_sessionContainer;

    /**
     * Sets up the session container
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->_sessionContainer = new SessionContainer(
            'hash',
            $this->createMock(SessionAdapterInterface::class)
        );
    }

    /**
     * Tests that the container creates a Session succesfuly
     *
     * @return Session
     */
    public function testGetSession()
    {
        $session1 = $this->_sessionContainer->getSession();
        $this->assertInstanceOf(Session::class, $session1);

        return $session1;
    }

    /**
     * Tests that the container returns the same session
     *
     * @param Session $session1 The generated session
     *
     * @return void

     * @depends testGetSession
     */
    public function testGetTheSameSession(Session $session1)
    {
        $session2 = $this->_sessionContainer->getSession();
        $this->assertEquals($session1, $session2);
    }
}

<?php

namespace Session\Model;

use PHPUnit\Framework\TestCase;
use WpNonce\Session\Model\JsonFileAdapter;

class JsonFileAdapterTest extends TestCase
{
    const FILE_PREFIX = __DIR__ . DIRECTORY_SEPARATOR . 'session_';
    const TEST_TOKEN = 'test';

    /**
     * The session store used for tests
     *
     * @var JsonFileAdapter
     */
    private $_sessionStore;

    /**
     * Sets up the SessionStore to be used in all the tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        $this->_sessionStore = new JsonFileAdapter(self::FILE_PREFIX);
        parent::setUp();
    }

    /**
     * Tests that the session gets automatically created with an empty array
     * as data when it does not exist
     *
     * @return void
     * @throws \Exception
     */
    public function testGetSessionWhenEmpty(): void
    {
        $fileName = self::FILE_PREFIX . self::TEST_TOKEN;
        if (file_exists($fileName)) {
            unlink($fileName);
        }

        $this->assertNotNull($this->_sessionStore->getSession(self::TEST_TOKEN));
    }

    /**
     * Tests that the session gets automatically created with an empty array
     * as data when it does not exist
     *
     * @return  void
     * @throws  \Exception
     * @depends testGetSessionWhenEmpty
     */
    public function testWriteOnEmptySession(): void
    {
        $testData = ['test' => 'test'];
        $this->_sessionStore->saveSession(self::TEST_TOKEN, $testData);

        $fileName = self::FILE_PREFIX . self::TEST_TOKEN;

        $this->assertJsonStringEqualsJsonFile($fileName, json_encode($testData));
    }



    /**
     * Tests that the session gets automatically created with an empty array
     * as data when it does not exist
     *
     * @return  void
     * @throws  \Exception
     * @depends testWriteOnEmptySession
     */
    public function testWriteOnExistingSession(): void
    {
        $testData = ['test' => 'test', 'test2' => 'test2'];
        $this->_sessionStore->saveSession(self::TEST_TOKEN, $testData);

        $fileName = self::FILE_PREFIX . self::TEST_TOKEN;

        $this->assertJsonStringEqualsJsonFile($fileName, json_encode($testData));
    }

    /**
     * Tests that the session gets automatically created with an empty array
     * as data when it does not exist
     *
     * @return  void
     * @throws  \Exception
     * @depends testWriteOnExistingSession
     */
    public function testRetrieveSession(): void
    {
        $testData = ['test' => 'test', 'test2' => 'test2'];
        $sessionData = $this->_sessionStore->getSession(self::TEST_TOKEN);

        $this->assertEquals($testData, $sessionData);
    }
}

<?php


namespace WpNonce\Nonce\Model;

/**
 * Hasher
 *
 * @package WpNonce\Nonce\Model
 * @author  Octavian Paralescu <me@octav.info>
 * @license GPLv2 https://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */
class HashingService implements HashingServiceInterface
{
    /**
     * The salt
     *
     * @var string
     */
    private $_salt;
    /**
     * The hashing algorithm
     *
     * @var string
     */
    private $_algorithm;

    /**
     * HashingService constructor.
     *
     * @param string $salt The salt
     * @param string $algo The algorithm
     */
    public function __construct(string $salt, string $algo)
    {
        $this->_salt = $salt;
        $this->_algorithm = $algo;
    }

    /**
     * Hashes a string
     *
     * @param string $string The string that we need to hash
     *
     * @return string
     */
    public function hash(string $string): string
    {
        return hash_hmac($this->_algorithm, $string, $this->_salt);
    }

    /**
     * Checks if two hashes are equal
     *
     * @param string $string1 Hash to be checked
     * @param string $string2 Hash to be checked against
     *
     * @return bool
     */
    public function hashEquals(string $string1, string $string2): bool
    {
        $string1Length = strlen($string1);
        if ($string1Length !== strlen($string2)) {
            return false;
        }
        $result = 0;

        // Do not attempt to "optimize" this. (Actual WP SourceCode comment)
        for ($i = 0; $i < $string1Length; $i++) {
            $result |= ord($string1[$i]) ^ ord($string2[$i]);
        }

        return $result === 0;
    }
}
<?php


namespace WpNonce\Nonce\Model;


use WpNonce\Session\Model\SessionContainer;

/**
 * The nonce service
 * Using mb_ functions because this should be a habit for PHP devs
 * not because it's required
 *
 * @package WpNonce\Nonce\Model
 * @author  Octavian Paralescu <me@octav.info>
 * @license GPLv2 https://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */
class NonceService
    implements NonceServiceInterface
{
    /**
     *  Status code for an invalid nonce
     */
    const NONCE_INVALID = 0;
    /**
     *  Status code for a nonce generated 0 to 12 hours ago
     */
    const NONCE_GENERATED_0_12_HOURS_AGO = 1;
    /**
     * Status code for a nonce generated 12 to 24 hours ago
     */
    const NONCE_GENERATED_12_24_HOURS_AGO = 2;
    /**
     * Nonce lifetime in seconds
     *
     * @var int
     */
    private $_nonceLifeTime;
    /**
     * The session container used to retrieve
     * the Session singleton
     *
     * @var SessionContainer
     */
    private $_sessionContainer;
    /**
     * The current date and time, used to generate
     * and verify the nonces in a time-dependent manner
     *
     * @var \DateTime
     */
    private $_currentDateTime;
    /**
     * The hasher
     *
     * @var HashingServiceInterface
     */
    private $_hasher;

    /**
     * NonceService constructor.
     *
     * @param int                     $nonceLifeTime    Nonce lifetime in seconds
     * @param SessionContainer        $sessionContainer The session container
     * @param HashingServiceInterface $hasher           The string hasher
     * @param \DateTime               $currentDateTime  Current date and time
     */
    public function __construct(
        int $nonceLifeTime,
        SessionContainer $sessionContainer,
        HashingServiceInterface $hasher,
        \DateTime $currentDateTime
    ) {
        $this->_nonceLifeTime = $nonceLifeTime;
        $this->_sessionContainer = $sessionContainer;
        $this->_currentDateTime = $currentDateTime;
        $this->_hasher = $hasher;
    }

    /**
     * Generates a nonce for an action
     *
     * @param string|null $action Specific action that requires a nonce/null for any
     *
     * @return string
     * @throws \Exception
     */
    public function generateNonce(string $action = null): string
    {

        if (!$session = $this->_sessionContainer->getSession()) {
            throw new \Exception('Internal error: could not create a session.');
        }

        if (!$user = $session->getUser()) {
            throw new \Exception('Auth error: user is not logged in.');
        }


        $userId = $user->getId();

        $dateInStringFormat = $this->_formatDate($this->_currentDateTime);

        $toBeHashed = $dateInStringFormat . '|'
            . $action . '|'
            . $userId . '|'
            . $session->getHash();

        $result = mb_substr($this->_hasher->hash($toBeHashed), -12, 10);

        return $result;
    }

    /**
     * Checks the Nonce if valid at this date
     *
     * @param string      $nonce  The nonce to be checked
     * @param string|null $action Specific action that requires a nonce/null for any
     *
     * @return bool
     * @throws \Exception
     */
    public function checkNonce(string $nonce, string $action = null): int
    {
        if (!$session = $this->_sessionContainer->getSession()) {
            throw new \Exception('Internal error: could not create a session.');
        }

        if (!$user = $session->getUser()) {
            throw new \Exception('Auth error: user is not logged in.');
        }

        $userId = $user->getId();

        $dateFormat = $this->_formatDate($this->_currentDateTime);

        // Nonce generated 0-12 hours ago
        $toBeHashed012HAgo = $dateFormat . '|'
            . $action . '|'
            . $userId . '|'
            . $session->getHash();
        $expected012 = substr(
            $this->_hasher->hash($toBeHashed012HAgo),
            -12,
            10
        );
        if ($this->_hasher->hashEquals($expected012, $nonce)) {
            return self::NONCE_GENERATED_0_12_HOURS_AGO;
        }

        // Nonce generated 12-24 hours ago
        $toBeHashed1224HAgo = ($dateFormat - 1) . '|'
            . $action . '|'
            . $userId . '|'
            . $session->getHash();

        $expected1224 = substr(
            $this->_hasher->hash($toBeHashed1224HAgo),
            -12,
            10
        );

        if ($this->_hasher->hashEquals($expected1224, $nonce)) {
            return self::NONCE_GENERATED_12_24_HOURS_AGO;
        }

        return self::NONCE_INVALID;
    }

    /**
     * Returns the date in the desired format
     *
     * @param \DateTime $date The date in the initial PHP class format
     *
     * @return int
     */
    private function _formatDate(\DateTime $date): int
    {
        return ceil($date->getTimestamp() / ($this->_nonceLifeTime / 2));
    }
}
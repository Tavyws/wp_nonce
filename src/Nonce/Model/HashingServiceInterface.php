<?php


namespace WpNonce\Nonce\Model;

/**
 * Hasher interface
 *
 * @package WpNonce\Nonce\Model
 * @author  Octavian Paralescu <me@octav.info>
 * @license GPLv2 https://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */
interface HashingServiceInterface
{
    /**
     * Hashes a string
     *
     * @param string $string The string that we need to hash
     *
     * @return string
     */
    public function hash(string $string): string;

    /**
     * Checks if two hashes are equal
     *
     * @param string $string1 Hash to be checked
     * @param string $string2 Hash to be checked against
     *
     * @return bool
     */
    public function hashEquals(string $string1, string $string2): bool;
}
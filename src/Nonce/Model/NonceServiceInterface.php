<?php


namespace WpNonce\Nonce\Model;


/**
 * Interface NonceServiceInterface
 *
 * @package WpNonce\Nonce\Model
 * @author  Octavian Paralescu <me@octav.info>
 * @license GPLv2 https://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */
interface NonceServiceInterface
{
    /**
     * Generates a nonce
     *
     * @param string|null $action Action for the nonce
     *
     * @return string
     */
    public function generateNonce(string $action = null): string;

    /**
     * Checks a nonce
     *
     * @param string      $nonce  The nonce to be checked
     * @param string|null $action The action
     *
     * @return int
     */
    public function checkNonce(string $nonce, string $action = null): int;
}
<?php


namespace WpNonce\Session\Model;

/**
 * Class JsonFileAdapter
 *
 * @package WpNonce\Session\Model
 * @author  Octavian Paralescu <me@octav.info>
 * @license GPLv2 https://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */
class JsonFileAdapter implements SessionAdapterInterface
{
    const SESSION_KEY = 'session_';
    private $_fileNamePrefix;

    /**
     * PhpSessionStore constructor.
     *
     * @param string $fileNamePrefix The path and the file prefix for session files
     */
    public function __construct($fileNamePrefix)
    {
        $this->_fileNamePrefix = $fileNamePrefix;
    }

    /**
     * Retrieves the session from a JSON file
     *
     * @param string $token The session token
     *
     * @return array
     * @throws \Exception
     */
    public function getSession(string $token): array
    {
        $fileName = $this->_getFileName($token);

        if (!file_exists($fileName)) {
            // Try to create the file
            $result = file_put_contents($fileName, '');

            if ($result === false) {
                throw new \Exception('Internal error: cannot write session file.');
            }

            return [];
        }

        $contents = file_get_contents($fileName);

        return json_decode($contents, true);
    }

    /**
     * Saves the session as JSON in a file
     *
     * @param string $token The session token
     * @param array  $data  The latest session data
     *
     * @return void
     */
    public function saveSession(string $token, array $data): void
    {
        file_put_contents($this->_getFileName($token), json_encode($data));
    }

    /**
     * Retrieves the filename using the path from the config and
     * the token from the session
     *
     * @param string $token The session token
     *
     * @return string
     */
    private function _getFileName($token)
    {
        return $this->_fileNamePrefix . $token;
    }
}
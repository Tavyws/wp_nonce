<?php


namespace WpNonce\Session\Model;


/**
 * Interface SessionStoreInterface
 * Should suffice getting a Session for reading and
 * saving a session after it gets changed
 *
 * @package Session\Model
 * @author  Octavian Paralescu <me@octav.info>
 * @license GPLv2 https://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */
interface SessionAdapterInterface
{
    /**
     * Retrieves the session data
     *
     * @param string $hash The session hash
     *
     * @return array
     */
    public function getSession(string $hash): array;

    /**
     * Saves the session data
     *
     * @param string $token The session token
     * @param array  $data  The new session data that needs to be saved
     *
     * @return void
     */
    public function saveSession(string $token, array $data): void;
}
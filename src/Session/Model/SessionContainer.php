<?php


namespace WpNonce\Session\Model;


use WpNonce\Session\Entity\Session;

/**
 * Class SessionContainer - created to keep in place
 * the singleton pattern for the Session
 *
 * @package Session\Model
 * @author  Octavian Paralescu <me@octav.info>
 * @license GPLv2 https://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */
class SessionContainer
{
    /**
     * The session hash
     *
     * @var string
     */
    private $_sessionHash;
    /**
     * The session store
     *
     * @var SessionAdapterInterface
     */
    private $_sessionStore;
    /**
     * The initialized Session object or null if uninitialized
     *
     * @var null|Session
     */
    private $_session;

    /**
     * SessionContainer constructor.
     *
     * @param string                  $sessionHash  The session hash
     * @param SessionAdapterInterface $sessionStore The session store
     */
    public function __construct(
        string $sessionHash,
        SessionAdapterInterface $sessionStore
    ) {
        $this->_sessionHash = $sessionHash;
        $this->_sessionStore = $sessionStore;
    }

    /**
     * Gets the session (singleton)
     *
     * @return Session
     */
    public function getSession(): Session
    {
        if (!$this->_session) {
            $this->_session = new Session($this->_sessionStore, $this->_sessionHash);
        }

        return $this->_session;
    }

}
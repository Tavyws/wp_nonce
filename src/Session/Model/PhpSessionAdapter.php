<?php


namespace WpNonce\Session\Model;

/**
 * Class PhpSessionAdapter
 * Used this adapter as the default and then realised it should be
 * tested using Selenium so I'm not using it anymore. In a real world
 * scenario this is probably faster than a JsonFileAdapter but slower
 * and less beneficial than a Memcached or Redis or even NoSQL Adapter
 *
 * @package WpNonce\Session\Model
 * @author  Octavian Paralescu <me@octav.info>
 * @license GPLv2 https://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */
class PhpSessionAdapter implements SessionAdapterInterface
{
    /**
     * Session prefix
     */
    const SESSION_KEY = 'session_';

    /**
     * PhpSessionStore constructor.
     */
    public function __construct()
    {
        session_start();
    }

    /**
     * Gets the session data
     *
     * @param string $token The actual session token
     *
     * @return array
     */
    public function getSession(string $token): array
    {
        session_start();

        if (!isset($_SESSION[self::SESSION_KEY . $token])) {
            $_SESSION[self::SESSION_KEY . $token] = [];
        }

        return $_SESSION[self::SESSION_KEY . $token];
    }

    /**
     * Saves the session data
     *
     * @param string $token The session token
     * @param array  $data  The session data
     *
     * @return void
     */
    public function saveSession(string $token, array $data): void
    {
        $_SESSION[self::SESSION_KEY . $token] = $data;
    }
}
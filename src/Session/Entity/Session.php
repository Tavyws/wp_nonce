<?php


namespace WpNonce\Session\Entity;


use WpNonce\Auth\Entity\User;
use WpNonce\Session\Model\SessionAdapterInterface;

/**
 * Class Session
 *
 * @package WpNonce\Session\Entity
 * @author  Octavian Paralescu <me@octav.info>
 * @license GPLv2 https://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */
class Session
{
    const USER_KEY = 'user';
    /**
     * Session hash
     *
     * @var string
     */
    private $_hash;
    /**
     * Data for the current session, lazy-loaded
     * from the SessionStore, only if needed
     *
     * @var array|null
     */
    private $_data = null;
    /**
     * The session store
     *
     * @var SessionAdapterInterface
     */
    private $_sessionStore;

    /**
     * Session constructor.
     *
     * @param SessionAdapterInterface $sessionStore The Session Store
     * @param string                  $hash         The Session Hash
     */
    public function __construct(SessionAdapterInterface $sessionStore, string $hash)
    {
        $this->_sessionStore = $sessionStore;
        $this->_hash = $hash;
    }

    /**
     * Gets the session hash
     *
     * @return string
     */
    public function getHash(): string
    {
        return $this->_hash;
    }

    /**
     * Sets an item in the session
     *
     * @param string $itemName  The key
     * @param mixed  $itemValue The value
     *
     * @return bool
     */
    public function setItem(string $itemName, $itemValue): bool
    {
        if ($itemValue) {
            $this->_data[$itemName] = $itemValue;
        } else {
            unset($this->_data[$itemName]);
        }

        // Trigger a changed event to save the session data
        $this->_changed();

        return true;
    }

    /**
     * Gets an item from the session
     *
     * @param string $itemName The key
     *
     * @return mixed|null
     */
    public function getItem(string $itemName)
    {
        return (array_key_exists($itemName, $this->_data))
            ? $this->_data[$itemName]
            : null;
    }

    /**
     * Checks if a user has been logged in
     *
     * @return bool
     */
    public function isLoggedIn(): bool
    {
        $user = $this->getUser();

        return $user && get_class($user) === User::class;
    }

    /**
     * Returns the logged in user (if any) or null
     *
     * @return User|null
     */
    public function getUser(): User
    {
        return $this->getItem(self::USER_KEY);
    }

    /**
     * Logs the user in
     *
     * @param int    $userId   The user numeric ID
     * @param string $userName The user name
     *
     * @return bool
     */
    public function logIn(int $userId, string $userName): bool
    {
        $user = new User($userId, $userName);

        return $this->setItem(self::USER_KEY, $user);
    }

    /**
     * Logs the user out
     *
     * @return void
     */
    public function logOut(): void
    {
        $this->setItem(self::USER_KEY, null);
    }

    /**
     * Event: Saves the session when it gets changed
     *
     * @return void
     */
    private function _changed(): void
    {
        $this->_sessionStore->saveSession($this->getHash(), $this->_data);
    }

    /**
     * Event: The session is interrogated, lazy-load the data
     * if necessary
     *
     * @return void
     */
    private function _interrogated(): void
    {
        if ($this->_data === null) {
            // Loads the data from the SessionStore or initializes it
            // as an empty array
            $this->_data = $this->_sessionStore->getSession($this->getHash());
        }
    }
}
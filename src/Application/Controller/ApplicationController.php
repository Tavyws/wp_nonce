<?php


namespace WpNonce\Application\Controller;


use WpNonce\Nonce\Model\NonceServiceInterface;
use WpNonce\Session\Model\SessionContainer;

/**
 * Class ApplicationController
 *
 * @package WpNonce\Application\Controller
 * @author  Octavian Paralescu <me@octav.info>
 * @license GPLv2 https://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */
class ApplicationController
{
    /**
     * The session container (singleton)
     *
     * @var SessionContainer
     */
    private $_sessionContainer;
    /**
     * The nonce generator/validator service
     *
     * @var NonceServiceInterface
     */
    private $_nonceService;

    /**
     * ApplicationController constructor.
     *
     * @param SessionContainer      $sessionContainer Session container dep
     * @param NonceServiceInterface $nonceService     The nonce service dep
     */
    public function __construct(
        SessionContainer $sessionContainer,
        NonceServiceInterface $nonceService
    ) {
        $this->_sessionContainer = $sessionContainer;
        $this->_nonceService = $nonceService;
    }

    /**
     * Logs the user in
     *
     * @param int    $userId   The user id
     * @param string $userName The user name
     *
     * @return bool
     */
    public function logIn(int $userId, string $userName): bool
    {
        $session = $this->_sessionContainer->getSession();

        return $session->logIn($userId, $userName);
    }

    /**
     * Creates a nonce
     *
     * @param string $action The nonce action
     *
     * @return string
     */
    public function createNonce(string $action)
    {
        return $this->_nonceService->generateNonce($action);
    }

    /**
     * Verify a nonce
     *
     * @param string $nonce  Nonce to be verified
     * @param string $action Action to be verified with a link to the nonce
     *
     * @return int
     */
    public function verifyNonce(string $nonce, string $action = null): int
    {
        return $this->_nonceService->checkNonce($nonce, $action);
    }

    /**
     * Logs the user out
     *
     * @return void
     */
    public function logOut(): void
    {
        $session = $this->_sessionContainer->getSession();

        $session->logOut();
    }
}
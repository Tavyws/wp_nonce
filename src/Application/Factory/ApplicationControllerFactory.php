<?php


namespace WpNonce\Application\Factory;


use WpNonce\Application\Controller\ApplicationController;
use WpNonce\Nonce\Model\HashingService;
use WpNonce\Nonce\Model\HashingServiceInterface;
use WpNonce\Nonce\Model\NonceService;
use WpNonce\Session\Model\JsonFileAdapter;
use WpNonce\Session\Model\PhpSessionAdapter;
use WpNonce\Session\Model\SessionAdapterInterface;
use WpNonce\Session\Model\SessionContainer;

/**
 * Class ApplicationControllerFactory
 *
 * @package WpNonce\Application\Factory
 * @author  Octavian Paralescu <me@octav.info>
 * @license GPLv2 https://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */
class ApplicationControllerFactory
{
    /**
     * Creates the Application Controller
     * Main DI entry point
     *
     * @param array     $sessionConfig   Session configuration
     * @param array     $hasher          Hasher configuration
     * @param array     $nonceConfig     Nonces configuration
     * @param string    $sessionKey      Current user's session key
     * @param \DateTime $currentDateTime DateTime used in the app
     *
     * @return ApplicationController
     * @throws \Exception
     */
    public static function create(
        array $sessionConfig,
        array $hasher,
        array $nonceConfig,
        string $sessionKey,
        \DateTime $currentDateTime
    ) {
        $sessionStore = self::_sessionAdapterFactory($sessionConfig);

        $hasher = self::_hasherFactory($hasher);

        $sessionContainer = new SessionContainer($sessionKey, $sessionStore);

        $nonceService = new NonceService(
            $nonceConfig['lifetime'],
            $sessionContainer,
            $hasher,
            $currentDateTime
        );

        return new ApplicationController($sessionContainer, $nonceService);
    }

    /**
     * Creates the session adapter based on the config
     *
     * @param array $sessionConfig Session configuration
     *
     * @return JsonFileAdapter|PhpSessionAdapter
     * @throws \Exception
     */
    private static function _sessionAdapterFactory(array $sessionConfig)
    : SessionAdapterInterface
    {
        // It's a switch statement because there could be others
        // in a real-life app such as:
        // redis, memcached, encrypted cookies, NoSQL DB etc
        switch ($sessionConfig['adapter']) {
        case 'php-session':
                $sessionStore = new PhpSessionAdapter();
            break;
        case 'json-file':
                $sessionStore = self::_jsonFileAdapterFactory($sessionConfig);
            break;
        default:
            throw new \Exception('Config error: session store type not supported.');
        }

        return $sessionStore;
    }

    /**
     * Creates the hasher based on the config
     *
     * @param array $hasherConfig Session configuration
     *
     * @return HashingService
     * @throws \Exception
     */
    private static function _hasherFactory(array $hasherConfig)
    : HashingServiceInterface
    {
        // White-listing algorithms
        switch ($hasherConfig['algo']) {
        case 'md5':
        case 'sha1':
            $hasher = new HashingService(
                $hasherConfig['salt'],
                $hasherConfig['algo']
            );
            break;
        default:
            throw new \Exception('Config error: hashing algorithm not supported.');
        }

        return $hasher;
    }

    /**
     * Json File Session Adapter Factory
     *
     * @param array $sessionConfig The config
     *
     * @return JsonFileAdapter
     * @throws \Exception
     */
    private static function _jsonFileAdapterFactory(array $sessionConfig)
    : JsonFileAdapter
    {
        if (!defined('DATA_FOLDER')) {
            throw new \Exception(
                'Internal error: could not find the data folder constant'
            );
        }

        $filePath
            = DATA_FOLDER . DIRECTORY_SEPARATOR . $sessionConfig['session_path'];
        $sessionStore = new JsonFileAdapter($filePath);

        return $sessionStore;
}
}
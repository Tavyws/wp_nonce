<?php


namespace WpNonce\Auth\Entity;


/**
 * Class User
 *
 * @package WpNonce\Auth\Entity
 * @author  Octavian Paralescu <me@octav.info>
 * @license GPLv2 https://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */
class User
{
    /**
     * User numeric ID
     *
     * @var int
     */
    private $_id;
    /**
     * User name
     *
     * @var string
     */
    private $_username;

    /**
     * User constructor.
     *
     * @param int    $id       User numeric ID
     * @param string $username User name
     */
    public function __construct(int $id, string $username)
    {
        $this->_id = $id;
        $this->_username = $username;
    }

    /**
     * Returns the user numeric ID
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->_id;
    }

    /**
     * Returns the user name
     *
     * @return string
     */
    public function getUsername(): string
    {
        return $this->_username;
    }
}